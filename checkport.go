package checkport

import (
	"fmt"
	"strconv"
)

func CheckPort(port string) error {
	portNum, err := strconv.Atoi(port)
	if err != nil {
		return fmt.Errorf("try to convert port '%s' to number: %w", port, err)
	}

	if 1 > portNum || portNum > 65535 {
		return fmt.Errorf("invalid port number '%d'. must be 1 < port < 65535", portNum)
	}

	return nil
}
